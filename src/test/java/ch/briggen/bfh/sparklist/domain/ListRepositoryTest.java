package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.ItemDBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.ItemDBTestHelper.initDataSourceForTest;
import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.NoSuchElementException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class ListRepositoryTest {
	

static ItemRepository repo = null;
static ListRepository listRepo = null;
static CategoryRepository categoryRepo = null;

	private static void populateRepos() {
		
		populateListRepo(listRepo);
		populateCategoryRepo(categoryRepo);
		populateItemRepo(repo);
		
	}

	private static void populateItemRepo(ItemRepository r) {
		for (int i = 0; i < 10; ++i) {
			long d = System.currentTimeMillis();
			Date myDate = new Date(d);
			Item dummy = new Item(i+1, "Fake Test Item" + i, i, "Beispieltext", 1, "Kühlschrank",1,
					myDate);
			r.insert(dummy);
		}
	}
	
	private static void populateListRepo(ListRepository r) {
		for (int i = 0; i < 10; ++i) {
			List dummy = new List(i+1, "Fake Test List" + i, "Das ist die " + i + ". Fake-Liste");
			r.insert(dummy);
		}
	}
	
	private static void populateCategoryRepo(CategoryRepository r) {
		r.CreateCategories();
	}

	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@" + System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		repo = new ItemRepository();
		listRepo = new ListRepository();
		categoryRepo = new CategoryRepository();
	}

	@Test
	void testEmptyDB() {
		assertThat("New DB must be empty", repo.getAll().isEmpty(), is(true));
	}

	@Test
	void testPopulatedDBWithItems() {
		populateRepos();
		assertThat("Freshly populated DB must hold 10 items", repo.getAll().size(), is(10));
	}

	@ParameterizedTest
	@CsvSource({ "300, TestListe 5,Das ist testliste 5 bruh", "301, TestListe 6,Das ist testliste 6 bruh", "302, TestListe 7,Das ist testliste 7 bruh"})
	void testGetById(int list_id, String list_name, String list_description) {
		
		populateRepos();
		
		ArrayList<Item> someItems = new ArrayList<Item>();
		
		for (int i = 0; i < 10; ++i) {
			long d = System.currentTimeMillis();
			Date myDate = new Date(d);
			Item dummy = new Item(i+1, "Fake Test Item" + i, i, "Beispieltext", 1, "Kühlschrank",1,
					myDate);
			someItems.add(dummy);
		}
		

		List i = new List(list_id, list_name,list_description, someItems);
		int dbId = listRepo.insert(i);
		List fromDB = listRepo.getById(dbId);
		assertThat("id = id", fromDB.getId(), is(dbId));
		assertThat("name = name", fromDB.getName(), is(list_name));
		assertThat("list_description = list_description", fromDB.getDescription(), is(list_description));

	}
	

	@ParameterizedTest
	@CsvSource({ "300, TestListe 5,Das ist testliste 5 bruh", "301, TestListe 6,Das ist testliste 6 bruh", "302, TestListe 7,Das ist testliste 7 bruh"})
	void testInsertLists(int list_id, String list_name, String list_description) {
		
		populateRepos();
		
		
		ArrayList<Item> someItems = new ArrayList<Item>();
		
		for (int i = 0; i < 10; ++i) {
			long d = System.currentTimeMillis();
			Date myDate = new Date(d);
			Item dummy = new Item(i+1, "Fake Test Item" + i, i, "Beispieltext", 1, "Kühlschrank",1,
					myDate);
			someItems.add(dummy);
		}
		

		List i = new List(list_id, list_name,list_description, someItems);
		int dbId = ListRepository.insert(i);
		List fromDB = listRepo.getById(dbId);
		
		assertThat("id = id", fromDB.getId(), is(dbId));
		assertThat("name = name", fromDB.getName(), is(list_name));
		assertThat("list_description = list_description", fromDB.getDescription(), is(list_description.toString()));

	}

	@ParameterizedTest
	@CsvSource({ "500,Erste Liste,das ist die erste Liste, erste Liste, upgedatede erste Liste", "600,Zweite Liste,das ist die zweite Liste,zweite Liste,upgedatede zweite Liste", "700,Dritte Liste,das ist die dritte Liste,dritte Liste,upgedatede dritte Liste" })
	void testSaveLists(int list_id, String list_name, String list_description, String new_list_name, String new_list_description) {
		
		populateRepos();

		List i = new List(list_id, list_name,list_description);
		int dbId = listRepo.insert(i);
		i.setId(dbId);
		i.setName(new_list_name);
		i.setDescription(new_list_description);
		listRepo.save(i);

		List fromDB = listRepo.getById(dbId);
		assertThat("id = id", fromDB.getId(), is(dbId));
		assertThat("name = name", fromDB.getName(), is(new_list_name));
		assertThat("count = count", fromDB.getDescription(), is(new_list_description));

	}

	@ParameterizedTest
	@CsvSource({ "600,Erste Liste,das ist die erste Liste", "601,Zweite Liste,das ist die zweite Liste", "602, Dritte Liste,das ist die dritte Liste" })
	void testDeleteItems(int list_id, String list_name, String list_description) {
		
		populateRepos();

		ArrayList<Item> someItems = new ArrayList<Item>();
		
		for (int i = 0; i < 10; ++i) {
			long d = System.currentTimeMillis();
			Date myDate = new Date(d);
			Item dummy = new Item(i+1, "Fake Test Item" + i, i, "Beispieltext", 1, "Kühlschrank",1,
					myDate);
			someItems.add(dummy);
		}
		
		List i = new List(list_id, list_name,list_description, someItems);
		int dbId = listRepo.insert(i);
		
		List fromDB = listRepo.getById(dbId);
		
		assertThat("List was written to DB", fromDB, not(nullValue()));
		
		listRepo.delete(dbId);
		
		assertThrows(NoSuchElementException.class, () -> {
			listRepo.getById(dbId);
		}, "List should have been deleted");
		
		
	}

	@Test
	void testDeleteManyRows() {
		populateRepos();
		for (List i : listRepo.getAllLists()) {
			repo.delete(i.getId());
		}

		assertThat("DB must be empty after deleteing all items", repo.getAll().isEmpty(), is(true));
		 
	}
	
	@Test
	void testCreateExampleLists() {
		
		ListRepository.createExampleLists();
		
		Collection<List> lists = listRepo.getAllLists();
		Iterator<List> iter = lists.iterator();
		List list1 = iter.next();
		List list2 = iter.next();
	
		assertThat("Example List 1 was created", list1,not(nullValue()));
		assertThat("Example List 1 has name", list1.getName(),not(nullValue()));
		assertThat("Example List 1 has descriptiojn", list1.getDescription(),not(nullValue()));
		
		assertThat("Example List 2 was created", list2,not(nullValue()));
		assertThat("Example List 2 has name", list2.getName(),not(nullValue()));
		assertThat("Example List 2 has descriptiojn", list2.getDescription(),not(nullValue()));
		 
	}
	


}
