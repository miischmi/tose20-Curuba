package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.ItemDBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.ItemDBTestHelper.initDataSourceForTest;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.Collection;
import java.util.Iterator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class ConfigRepositoryTest {
	
ConfigRepository confRepo;
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@" + System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		confRepo = new ConfigRepository();
		
	}
	
	@Test
	void InitializeRepo() {
        assertThat(confRepo, is(notNullValue()));
	}
	
	@Test
	void testGetAndInsertConfig() {
		
		Config theC = new Config(true);
		
		int dbId = confRepo.insertConfig(theC);
		Collection<Config> theConfigs = confRepo.getConfig();
		
		Iterator<Config> iter = theConfigs.iterator();
		Config test = iter.next();
		
		assertThat("id = id", test.getId(), is(dbId));
		assertThat("startup = startup", test.getFirstStartup(), is(true));
		
	}
	
	
	@Test
	void testSaveConfig() {
		
		Config theC = new Config(true);
		
		int dbId = confRepo.insertConfig(theC);
		Collection<Config> theConfigs = confRepo.getConfig();
		
		Iterator<Config> iter = theConfigs.iterator();
		Config test = iter.next();
		
		assertThat("id = id", test.getId(), is(dbId));
		assertThat("startup = startup", test.getFirstStartup(), is(true));
		
		test.setFirstStartup(false);
		
		confRepo.saveConfig(test);
		Collection<Config> theConfigs2 = confRepo.getConfig();
		
		Iterator<Config> iter2 = theConfigs2.iterator();
		Config test2 = iter2.next();
		

		assertThat("id = id", test2.getId(), is(dbId));
		assertThat("startup = startup", test2.getFirstStartup(), is(false));
	
	}
	
	

}
