package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.ItemDBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.ItemDBTestHelper.initDataSourceForTest;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import java.sql.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class ItemTest {

	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@" + System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
	}
	
	@Test
	void testEmptyContructorYieldsEmptyObject() {
		Item i = new Item();
		assertThat("Id",i.getId(),is(equalTo(0)));
		assertThat("Name",i.getName(),is(equalTo(null)));
		assertThat("Stock",i.getStock(),is(equalTo(0)));
		assertThat("Description",i.getDescription(),is(equalTo(null)));
		assertThat("CategoryId",i.getCategoryId(),is(equalTo(0)));
		assertThat("StorageLocation",i.getStoragelocation(),is(equalTo(null)));
		assertThat("Expirydate",i.getExpirydate(),is(equalTo(null)));
	}
	
	
	
	//@ParameterizedTest
	@CsvSource({"1,One,-1,somedescription,1,somelocation, 1, 1970-12-04","2,Two,0,somedescription,2,somelocation, 1,1970-12-04","3,Three,1,somedescription,3,somelocation, 1,1970-12-04"})
	void testContructorAssignsAllFields(int id,String name, int stock, String description, int categoryId, String storagelocation, int list_id, Date expirydate) {
		Item i = new Item(id, name, stock, description, categoryId, storagelocation, list_id,expirydate);
		assertThat("Id",i.getId(),is(equalTo(0l)));
		assertThat("Name",i.getName(),is(equalTo(null)));
		assertThat("Stock",i.getStock(),is(equalTo(0)));
		assertThat("Description",i.getDescription(),is(equalTo(null)));
		assertThat("CategoryId",i.getCategory(),is(equalTo(null)));
		assertThat("StorageLocation",i.getStoragelocation(),is(equalTo(null)));
		assertThat("List",i.getList(),is(equalTo(0)));
		assertThat("Expirydate",i.getExpirydate(),is(equalTo(null)));
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,-1,somedescription,1,somelocation, 1,1970-12-04","2,Two,0,somedescription,2,somelocation, 1,1970-12-04","3,Three,1,somedescription,3,somelocation, 1,1970-12-04"})
	void testSetters(int id,String name, int stock, String description, int categoryId, String storagelocation, int list_id, Date expirydate) {
		Item i = new Item();
		i.setId(id);
		i.setName(name);
		i.setStock(stock);
		i.setDescription(description);
		i.setCategoryId(categoryId);
		i.setStoragelocation(storagelocation);
		i.setList(list_id);
		i.setExpirydate(expirydate);
		
		System.out.println(i);
		
		assertThat("Id",i.getId(),is(equalTo(id)));
		assertThat("Name",i.getName(),is(equalTo(name)));
		assertThat("Stock",i.getStock(),is(equalTo(stock)));
		assertThat("Description",i.getDescription(),is(equalTo(description)));
		assertThat("CategoryId",i.getCategoryId(),is(equalTo(categoryId)));
		assertThat("StorageLocation",i.getStoragelocation(),is(equalTo(storagelocation)));
		assertThat("List_id",i.getList(),is(equalTo(list_id)));
		assertThat("Expirydate",i.getExpirydate(),is(equalTo(expirydate)));
		
	}
	
	@Test
	void testToString() {
		Item i = new Item();
		System.out.println(i.toString());
		assertThat("toString smoke test",i.toString(),not(nullValue()));
	}
}
