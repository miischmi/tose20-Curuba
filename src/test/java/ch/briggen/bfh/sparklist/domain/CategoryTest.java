package ch.briggen.bfh.sparklist.domain;

import static org.hamcrest.CoreMatchers.is;
import static ch.briggen.bfh.sparklist.domain.ItemDBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.ItemDBTestHelper.initDataSourceForTest;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;


public class CategoryTest {
	
	CategoryRepository catRepo = null;

	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@" + System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		catRepo = new CategoryRepository();
		catRepo.CreateCategories();
	}
	
	@Test
	void InitializeRepo() {
        assertThat(catRepo, is(notNullValue()));
	}
	
	@Test
	void testCreateCategories() {
		assertThat("CategoryRepo holds 5 hardcoded categories", catRepo.getAll().size(), is(5));
	}
	
	
	@Test
	void testGetById() {
		Category firstCategory = catRepo.getById(1);
		String categoryName = firstCategory.getName();
	    assertThat("Got correct category from categoryRepo", categoryName, containsString("Lebensmittel (Raumtemperatur)"));
	}
	
	
	@ParameterizedTest
	@CsvSource({ "1,Vogelfutter,Futter für die Vögel", "2, Hundefutter, Futter für die Hunde", "3, Katzenfutter, Futter für die Katzen"})
	void testGetCategorySetters(Integer id, String name, String description) {
	
		Category c = new Category();
		
		c.setDescription(description);
		c.setId(id);
		c.setName(name);
		
		
		assertThat("ID is the same", c.getId(), is(id));
		assertThat("Name is the same", c.getName(), is(name));
		assertThat("Description is the same", c.getDescription(), is(description));
		
	}
	
	@ParameterizedTest
	@CsvSource({ "1,Vogelfutter,Futter für die Vögel", "2, Hundefutter, Futter für die Hunde", "3, Katzenfutter, Futter für die Katzen"})
	void testGetCategoryGetters(Integer id, String name, String description) {
	
		Category c = new Category();
		
		c.setDescription(description);
		c.setId(id);
		c.setName(name);
		
		int idGet = c.getId();
		String nameGet = c.getName();
		String descGet = c.getDescription();
		
		
		assertThat("ID is the same", idGet, is(id));
		assertThat("Name is the same", nameGet, is(name));
		assertThat("Description is the same", descGet, is(description));
		
	}
	
	
	
}
