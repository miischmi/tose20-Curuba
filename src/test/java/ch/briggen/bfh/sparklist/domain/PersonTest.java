package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.ItemDBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.ItemDBTestHelper.initDataSourceForTest;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PersonTest {
	
	PersonRepository persRepo = null;

	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@" + System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		persRepo = new PersonRepository();
		
	}
	
	@Test
	void InitializeRepo() {
        assertThat(persRepo, is(notNullValue()));
	}
	
	@Test
	void testConstructors() {
        Person myPerson = new Person(1, "Hans" , "Mueller",  "hansmueller@mueller.com", 1);
        Person myPerson2 = new Person("Hans" , "Mueller",  "hansmueller@mueller.com", 1);
		
       
        assertThat("Id is ok", myPerson.getId(), is(1));
        assertThat("Firstname is ok", myPerson.getFirstname(), is("Hans"));
        assertThat("Firstname is ok", myPerson2.getFirstname(), is("Hans"));
        assertThat("Lastname is ok", myPerson.getLastname(), is("Mueller"));
        assertThat("Lastname is ok", myPerson2.getLastname(), is("Mueller"));
        assertThat("Email is ok", myPerson.getEmail(), is("hansmueller@mueller.com"));
        assertThat("Email is ok", myPerson2.getEmail(), is("hansmueller@mueller.com"));
        assertThat("impressum is ok", myPerson.getImpressum(), is(1));
        assertThat("impressum is ok", myPerson2.getImpressum(), is(1));
        assertThat("fullname is ok", myPerson.getFullName(), is("Hans Mueller"));
        assertThat("fullname is ok", myPerson2.getFullName(), is("Hans Mueller"));
 
        
	}
	
	
	@Test
	void testGettersAndSetters() {
		 Person myPerson = new Person(1, "Hans" , "Mueller",  "hansmueller@mueller.com", 1);
		 
		 myPerson.setEmail("www@www.www");
		 myPerson.setLastname("Buehler");
		 myPerson.setFirstname("Peter");
		 myPerson.setId(2);
		 myPerson.setImpressum(2);
		 
		 assertThat("Id is ok", myPerson.getId(), is(2));
	     assertThat("Firstname is ok", myPerson.getFirstname(), is("Peter"));
	     assertThat("Lastname is ok", myPerson.getLastname(), is("Buehler"));
	     assertThat("Email is ok", myPerson.getEmail(), is("www@www.www"));
	     assertThat("Impressum is ok", myPerson.getImpressum(), is(2));
	     
       
	}
	
	
	
	
	
	
	
	
	
	

}
