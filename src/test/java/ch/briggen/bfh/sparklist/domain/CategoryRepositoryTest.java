package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.ItemDBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.ItemDBTestHelper.initDataSourceForTest;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.sql.Date;
import java.util.NoSuchElementException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class CategoryRepositoryTest {
	
	static ItemRepository repo = null;
	static ListRepository listRepo = null;
	static CategoryRepository categoryRepo = null;

		private static void populateRepos() {
			
			populateListRepo(listRepo);
			populateCategoryRepo(categoryRepo);
			populateItemRepo(repo);
			
		}

		
		private static void populateItemRepo(ItemRepository r) {
			for (int i = 0; i < 10; ++i) {
				long d = System.currentTimeMillis();
				Date myDate = new Date(d);
				Item dummy = new Item(i+1, "Fake Test Item" + i, i, "Beispieltext", 1, "Kühlschrank",1,
						myDate);
				r.insert(dummy);
			}
		}
		
		private static void populateListRepo(ListRepository r) {
			for (int i = 0; i < 10; ++i) {
				List dummy = new List(i+1, "Fake Test List" + i, "Das ist die " + i + ". Fake-Liste");
				r.insert(dummy);
			}
		}
		
		private static void populateCategoryRepo(CategoryRepository r) {
			r.CreateCategories();
		}

	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@" + System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		repo = new ItemRepository();
		listRepo = new ListRepository();
		categoryRepo = new CategoryRepository();
	}
	
	@ParameterizedTest
	@CsvSource({ "Vogelfutter,Futter für die Vögel", "Hundefutter, Futter für die Hunde", "Katzenfutter, Futter für die Katzen"})
	void testInsertCategory(String name, String description) {
		populateRepos();
		Category c = new Category(name,description);
		int dbId = categoryRepo.insert(c);
		Category fromDB = categoryRepo.getById(dbId);
		
		assertThat("id = id", fromDB.getId(), is(dbId));
		assertThat("name = name", fromDB.getName(), is(name));
		assertThat("description = description", fromDB.getDescription(), is(description));
	}

	
	@ParameterizedTest
	@CsvSource({ "Vogelfutter,Futter für die Vögel", "Hundefutter, Futter für die Hunde", "Katzenfutter, Futter für die Katzen"})
	void testDeleteCategory(String name, String description) {
		populateRepos();

		Category c = new Category(name,description);
		int dbId = categoryRepo.insert(c);
		Category fromDB = categoryRepo.getById(dbId);
		
		assertThat("Category was written to DB", fromDB, not(nullValue()));
		categoryRepo.delete(dbId);
	
		assertThrows(NoSuchElementException.class, () -> {
			categoryRepo.getById(dbId);
		}, "Category should have been deleted");
	}
	
	
	@ParameterizedTest
	@CsvSource({ "Vogelfutter,Futter für die Vögel", "Hundefutter, Futter für die Hunde", "Katzenfutter, Futter für die Katzen"})
	void testGetCategoryById(String name, String description) {
		populateRepos();

		Category c = new Category(name,description);
		int dbId = categoryRepo.insert(c);
		Category fromDB = categoryRepo.getById(dbId);
		
		assertThat("id = id", fromDB.getId(), is(dbId));
		assertThat("name = name", fromDB.getName(), is(name));
		assertThat("description = description", fromDB.getDescription(), is(description));
	}
	
	@ParameterizedTest
	@CsvSource({ "Vogelfutter,Futter für die Vögel", "Hundefutter, Futter für die Hunde", "Katzenfutter, Futter für die Katzen"})
	void testGetCategoryByName(String name, String description) {
		populateRepos();

		Category c = new Category(name,description);
		int dbId = categoryRepo.insert(c);
		Category fromDBbyID = categoryRepo.getById(dbId);
		
		Category fromDBbyName = categoryRepo.getByName(fromDBbyID.getName());
		
		assertThat("ID is the same", fromDBbyID.getId(), is(fromDBbyName.getId()));
		assertThat("Name is the same", fromDBbyID.getName(), is(fromDBbyName.getName()));
		assertThat("Description is the same", fromDBbyID.getDescription(), is(fromDBbyName.getDescription()));
		
	}
	

}
