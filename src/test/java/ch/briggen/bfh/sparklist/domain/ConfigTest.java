package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.ItemDBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.ItemDBTestHelper.initDataSourceForTest;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ConfigTest {
	
	ConfigRepository confRepo;
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@" + System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		confRepo = new ConfigRepository();
	}
	
	@Test
	void InitializeRepo() {
        assertThat(confRepo, is(notNullValue()));
	}
	
	@Test
	void testIsSingleton() {
	
		Config myConfig = new Config(true);
		
		assertThat("Is correct instance", myConfig.getInstance(), is(Config.getInstance()));
		assertThat("Startup is ture", Config.getInstance().getFirstStartup(), is(true));
		
	}
	
	@Test
	void testConstructor() {
	
		Config myConfig = new Config(1, true);
		
		assertThat("startup is false", Config.getInstance().getFirstStartup(), is(true));
		assertThat("id is ok", Config.getInstance().getId(), is(1));
		
	}
	
	@Test
	void testStartUp() {
		
		Config myConfig = new Config(true);
		
		assertThat("Startup is true", Config.getInstance().getFirstStartup(), is(true));
		
	}
	
	@Test
	void testSettersAndGetters() {
		
		Config myConfig = new Config(true);
		Config.getInstance().setId(1);
		Config.getInstance().setFirstStartup(false);
		
		assertThat("startup is false", Config.getInstance().getFirstStartup(), is(false));
		assertThat("id is ok", Config.getInstance().getId(), is(1));
		
	
	}

}
