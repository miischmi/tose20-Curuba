package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.ItemDBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.ItemDBTestHelper.initDataSourceForTest;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import java.sql.Date;
import java.util.Collection;
import java.util.Iterator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class PersonRepositoryTest {
	
	static PersonRepository persRepo = null;
	static ImpressumRepository impRepo = null;
	
	
	private static void populateRepos() {
		
		populatePersonRepo(persRepo);
		
	}

	
	private static void populatePersonRepo(PersonRepository r) {
		
		Person michelle = new Person("Michelle", "Schmid", "michelle.schmid@students.bfh.ch", 1);
		Person carol = new Person("Carol", "Gutknecht", "carolann.gutknecht@students.bfh.ch", 1);
		Person marcel = new Person("Marcel", "Ernst", "marcel.ernst@students.bfh.ch", 1);
		Person denise = new Person("Denise", "Schärer", "denise.schaerer@students.bfh.ch", 1);
		Person dominique = new Person("Dominique", "Marty", "dominiquesebastian.marty@students.bfh.ch", 1);
		Person jonas = new Person("Jonas", "Baschung", "jonasmartin.baschung@students.bfh.ch", 1);

		persRepo.insertPerson(michelle);
		persRepo.insertPerson(carol);
		persRepo.insertPerson(marcel);
		persRepo.insertPerson(denise);
		persRepo.insertPerson(dominique);
		persRepo.insertPerson(jonas);
		
	}

	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@" + System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		persRepo = new PersonRepository();
		impRepo= new ImpressumRepository();
		CreateImprint();
	}
	
	private void CreateImprint() {
		Impressum imp = new Impressum(1, "Impressum", "impressum");
		impRepo.insertImpressum(imp);
		
	}


	@Test
	void InitializeRepo() {
        assertThat(persRepo, is(notNullValue()));
	}
	
	
	@ParameterizedTest
	@CsvSource({"hene, pesche, hene.pesche@students.bfh.ch, 1, ","hene2, pesche2, hene2.pesche2@students.bfh.ch, 1,", "hene3, pesche3, hene3.pesche3@students.bfh.ch, 1, " })
	void testInsertPersons(String firstname, String lastname, String email, int imprint) {
		
		Person p = new Person(firstname, lastname, email, imprint);
		int dbId = persRepo.insertPerson(p);
		
;		Collection<Person> personsFromDB = persRepo.getAllPersons();

		Iterator<Person> iter = personsFromDB.iterator();
		Person person = iter.next();
		
	
		assertThat("Contains 1 person", personsFromDB.size(), is(1));
		assertThat("person has correct firstname", person.getFirstname(), is(firstname));
		assertThat("person has correct lastname", person.getLastname(), is(lastname));
		assertThat("person has correct email", person.getEmail(), is(email));
		assertThat("person has correct firstname", person.getImpressum(), is(1));
		
	}
	
	
	@Test
	void getAllPersons() {
		
		populateRepos();
		
		Collection<Person> personsFromDB = persRepo.getAllPersons();

		Iterator<Person> iter = personsFromDB.iterator();
		Person michelle = iter.next();
		Person carol = iter.next();
		Person marcel = iter.next();
		Person denise = iter.next();
		Person dominique = iter.next();
		Person jonas = iter.next();
	
		assertThat("person is person", michelle.getFullName(), is("Michelle Schmid"));
		assertThat("has 6 persons", personsFromDB.size(), is(6));
		
	}
	
	@Test
	void AddTeam() {
        assertThat(persRepo, is(notNullValue()));
	}

	
	
	

}
