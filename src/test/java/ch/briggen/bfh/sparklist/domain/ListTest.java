package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.ItemDBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.ItemDBTestHelper.initDataSourceForTest;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class ListTest {
	
	ListRepository listRepo = null;
	Collection<Item> someItems;
	Collection<List> someLists;
	

	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@" + System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		listRepo = new ListRepository();
		createStuff();
	}
	
	@Test
	void InitializeRepo() {
        assertThat(listRepo, is(notNullValue()));
	}
	
	void createStuff() {
		
		 someItems = new ArrayList<Item>();
			
			for (int i = 0; i < 10; ++i) {
				long d = System.currentTimeMillis();
				Date myDate = Date.valueOf("1970-12-04");
				Item dummy = new Item();
				someItems.add(dummy);
			}
		
			// create some lists
		     someLists = new ArrayList<List>();
			
			for (int i = 0; i < 10; ++i) {
				List dummy = new List(i+1, "Fake Test List" + i, "Das ist die " + i + ". Fake-Liste");
				someLists.add(dummy);
			}
	}

	@Test
	void testCreateLists() {
		
	   List list1 = new List();
	   List list2 = new List(2, "Liste 2", "Das ist Liste 2", someItems);
	   List list3 = new List(3, "Liste 3", "Das ist Liste 3");
	   List list4 = new List(4,"Liste 4", "Das ist Liste 4", someItems, someLists);
	   List list5 = new List("Liste 5","Das ist Liste 5");
	  
	   assertThat("List 1 is ok", list1, not(nullValue()));
	   assertThat("List 2 is ok", list2, not(nullValue()));
	   assertThat("List 3 is ok", list3, not(nullValue()));
	   assertThat("List 4 is ok", list4, not(nullValue()));
	   assertThat("List 5 is ok", list5, not(nullValue()));
	    
	   
	}
	
	@ParameterizedTest
	@CsvSource({ "1,Liste 1, Das ist Liste 1", "2,Liste 2, Das ist Liste 2", "3,Liste 3, Das ist Liste 3"})
	void testListSetters(Integer id, String name, String description) {
	
		List l = new List();
		
		l.setDescription(description);
		l.setId(id);
		l.setName(name);
		l.setItems(someItems);
		l.setAllLists(someLists);
		
		assertThat("ID is the same", l.getId(), is(id));
		assertThat("Name is the same", l.getName(), is(name));
		assertThat("Description is the same", l.getDescription(), is(description));
		assertThat("Items are the same", l.getItems(), is(someItems));
		assertThat("Lists is the same", l.getAllLists(), is(someLists));
		
	}

	

	

}
