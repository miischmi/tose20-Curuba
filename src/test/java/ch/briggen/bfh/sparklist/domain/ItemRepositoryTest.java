package ch.briggen.bfh.sparklist.domain;

import static org.hamcrest.CoreMatchers.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import static ch.briggen.bfh.sparklist.domain.ItemDBTestHelper.*;

import java.sql.Date;
import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.Calendar;
import java.time.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class ItemRepositoryTest {

	static ItemRepository repo = null;
	static ListRepository listRepo = null;
	static CategoryRepository categoryRepo = null;

		private static void populateRepos() {
			
			populateListRepo(listRepo);
			populateCategoryRepo(categoryRepo);
			populateItemRepo(repo);
			
		}

		
		private static void populateItemRepo(ItemRepository r) {
			for (int i = 0; i < 10; ++i) {
				long d = System.currentTimeMillis();
				Date myDate = new Date(d);
				Item dummy = new Item(i+1, "Fake Test Item" + i, i, "Beispieltext", 1, "Kühlschrank",1,
						myDate);
				r.insert(dummy);
			}
		}
		
		private static void populateListRepo(ListRepository r) {
			for (int i = 0; i < 10; ++i) {
				List dummy = new List(i+1, "Fake Test List" + i, "Das ist die " + i + ". Fake-Liste");
				r.insert(dummy);
			}
		}
		
		private static void populateCategoryRepo(CategoryRepository r) {
			r.CreateCategories();
		}

	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@" + System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		repo = new ItemRepository();
		listRepo = new ListRepository();
		categoryRepo = new CategoryRepository();
	}

	@Test
	void testEmptyDB() {
		assertThat("New DB must be empty", repo.getAll().isEmpty(), is(true));
	}

	@Test
	void testPopulatedDB() {
		populateRepos();
		assertThat("Freshly populated DB must hold 10 items", repo.getAll().size(), is(10));
	}

	@ParameterizedTest
	@CsvSource({ "1,One,-1,test1,1,somelocation,1,1970-12-04", "2,Two,0,test2,2,somelocation,1,1970-12-04", "3,Three,1,test3,3,somelocation,1,1970-12-04"})
	void testInsertItems(int id, String name, int stock, String description, int category, String storagelocation,
			int list_id, Date expirydate) {
		populateRepos();
		Item i = new Item(id, name, stock, description, category, storagelocation,list_id, expirydate);
		int dbId = repo.insert(i);
		Item fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(), is(dbId));
		assertThat("name = name", fromDB.getName(), is(name));
		assertThat("count = count", fromDB.getStock(), is(stock));
		assertThat("description = description", fromDB.getDescription(), is(description));
		assertThat("category = category", fromDB.getCategory().getId(), is(category));
		assertThat("storagelocation = storagelocation", fromDB.getStoragelocation(), is(storagelocation));
		assertThat("expirydate = expirydate", fromDB.getExpirydate(), is(expirydate));
		assertThat("expirydate = expirydate", fromDB.getList(), is(list_id));

	}

	@ParameterizedTest
	@CsvSource({ "1,One,-1,somedescription, 1,somelocation,1,1970-12-04, NewName, 2", "2,Two,0,somedescription,2,somelocation,1,1970-12-04, NewName2, 3", "3,Three,1,somedescription, 3,somelocation,1,1970-12-04, NewName3, 5" })
	void testUpdateItems(int id, String name, int stock, String description, int category, String storagelocation,int list_id,
			Date expirydate, String newName, int newQty) {
		
		populateRepos();
		Item i = new Item(id, name, stock, description, category, storagelocation, list_id,expirydate);
		int dbId = repo.insert(i);
		i.setId(dbId);
		i.setName(newName);
		i.setStock(newQty);
		repo.save(i);

		Item fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(), is(dbId));
		assertThat("name = name", fromDB.getName(), is(newName));
		assertThat("count = count", fromDB.getStock(), is(newQty));

	}

	@ParameterizedTest
	@CsvSource({ "1,One,-1,test1,1,somelocation,1,1970-12-04", "2,Two,0,test2,2,somelocation,1,1970-12-04", "3,Three,1,test3,3,somelocation,1,1970-12-04"})
	void testDeleteItems(int id, String name, int stock, String description, int category, String storagelocation,int list_id,
			Date expirydate) {
		populateRepos();

		Item i = new Item(id, name, stock, description, category, storagelocation, list_id,expirydate);
		int dbId = repo.insert(i);
		Item fromDB = repo.getById(dbId);
		assertThat("Item was written to DB", fromDB, not(nullValue()));
		repo.delete(dbId);
		assertThrows(NoSuchElementException.class, () -> {
			repo.getById(dbId);
		}, "Item should have been deleted");
	}

	@Test
	void testDeleteManyRows() {
		populateRepos();
		for (Item i : repo.getAll()) {
			repo.delete(i.getId());
		}

		assertThat("DB must be empty after deleteing all items", repo.getAll().isEmpty(), is(true));
	}

	@ParameterizedTest
	@CsvSource({ "1,One,-1,somedescription,2,somelocation,1,1970-12-04", "2,Two,0,somedescription,1,somelocation,1,1970-12-04", "3,Three,1,somedescription,3,somelocation,1,1970-12-04" })
	void testGetByOneName(int id, String name, int stock, String description, int category, String storagelocation,int list_id,
			Date expirydate) {
		populateRepos();

		Item i = new Item(id, name, stock, description, category, storagelocation, list_id,expirydate);
		repo.insert(i);
		Collection<Item> fromDB = repo.getByName(name);
		assertThat("Exactly one item was returned", fromDB.size(), is(1));

		Item elementFromDB = fromDB.iterator().next();

		assertThat("name = name", elementFromDB.getName(), is(name));
		assertThat("count = count", elementFromDB.getStock(), is(stock));
	}

	@ParameterizedTest
	@CsvSource({ "1,One,-1,somedescription,3,somelocation,1,1970-12-04", "2,Two,0,somedescription,2,somelocation,1,1970-12-04", "3,Three,1,somedescription,1,somelocation,1,1970-12-04" })
	void testGetManyItemsByName(int count, String name, int stock, String description, int category, String storagelocation,int list_id,
			Date expirydate) {
		populateRepos();

		for (int n = 0; n < count; ++n) {
			Item i = new Item(0, name, stock, description, category, storagelocation, list_id, expirydate );
			repo.insert(i);
		}

		Collection<Item> fromDB = repo.getByName(name);
		assertThat("Exactly one item was returned", fromDB.size(), is(count));

		for (Item elementFromDB : fromDB) {
			assertThat("name = name", elementFromDB.getName(), is(name));
			assertThat("count = count", elementFromDB.getStock(), is(stock));
		}
	}

	@Test
	void testGetNoItemsByName() {
		populateRepos();

		Collection<Item> fromDB = repo.getByName("NotExistingItem");
		assertThat("Exactly one item was returned", fromDB.size(), is(0));

	}
	
	@Test
	void testDate() {
		populateRepos();
		
		//Get first item of repo, compare date of item with date today
		Item fromDB = repo.getById(1);
		Calendar calendar = Calendar.getInstance();
		LocalDate todayLocalDate = LocalDate.now( ZoneId.of( "Europe/Zurich" ));
		java.sql.Date today = java.sql.Date.valueOf(todayLocalDate);
		assertThat("Date is correct", fromDB.getExpirydate(), is(today));

	}
}
