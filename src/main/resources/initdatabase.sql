--Drop tables for debug reasons
--DROP TABLE if exists items;
--DROP TABLE if exists lists;
--DROP TABLE if exists categories;
--DROP TABLE if exists person;
--DROP TABLE if exists impressum;
--DROP TABLE if exists config;




create table if not exists impressum (impressum_id INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
	impressum_name varchar(200), impressum_description varchar(200));

create table if not exists person (person_id INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
	person_firstname varchar(200), person_lastname varchar(200),  email varchar(200), impressum INTEGER,
	FOREIGN KEY (impressum) REFERENCES impressum(impressum_id));
	
create table if not exists config (config_id INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
	config_firststartup BOOLEAN DEFAULT FALSE);

create table if not exists categories (category_id INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
	category_name varchar(200), category_description varchar(200));

create table if not exists lists (list_id INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
	list_name varchar(200), list_description varchar(200));

create table if not exists items (item_id INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
	item_name varchar(200), stock decimal, item_description varchar(200),
	category INTEGER, storagelocation varchar(200), list INTEGER, expirydate DATETIME,
	FOREIGN KEY (category) REFERENCES categories(category_id), FOREIGN KEY (list) REFERENCES lists(list_id));



