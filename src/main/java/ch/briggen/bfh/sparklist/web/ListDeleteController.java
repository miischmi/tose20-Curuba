package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ListRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class ListDeleteController implements TemplateViewRoute {

	private final Logger log = LoggerFactory.getLogger(ListDeleteController.class);

	private ListRepository listRepo = new ListRepository();

	/**
	 * Löscht die Liste mit der übergebenen ID aus der Datenbank
	 * 
	 * Hört auf GET /list/delete
	 * 
	 * @return Redirect zurück zur Liste
	 */
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /list/delete mit id " + id);
		int longId = Integer.parseInt(id);
		listRepo.delete(longId);
		response.redirect("/");
		return null;
	}

}
