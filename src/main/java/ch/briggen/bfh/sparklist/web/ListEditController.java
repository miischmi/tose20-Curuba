package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.*;
import ch.briggen.bfh.sparklist.domain.List;
import spark.ModelAndView;
import spark.Request;
import java.util.*;
import spark.Response;
import spark.TemplateViewRoute;

public class ListEditController implements TemplateViewRoute {

	private final Logger log = LoggerFactory.getLogger(ListEditController.class);

	private ListRepository listRepo = new ListRepository();
	private CategoryRepository catRepo = new CategoryRepository();

	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {

		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
		String[] errors = request.queryParamsValues("error");

		if (null == idString || idString.equals("0")) {
			log.trace("GET /list für INSERT mit id " + idString);
			model.put("postAction", "/list/new");
			model.put("backButton", "/");
			model.put("listDetail", new List());
			model.put("errors", errors);
			return new ModelAndView(model, "newListDetailTemplate");
		}

		else {
			log.trace("GET /list für UPDATE mit id " + idString);
			model.put("postAction", "/list/update");

			// Filter

			Collection<Category> categories = catRepo.getAll();
			model.put("categories", categories);
			model.put("errors", errors);

			// List
			int id = Integer.parseInt(idString);
			List i = listRepo.getById(id);
			model.put("backButton", "/");
			model.put("listTitle", i.getName());
			model.put("listDetail", i);
			model.put("list", i.getItems());
			return new ModelAndView(model, "listDetailTemplate");
		}

	}
}
