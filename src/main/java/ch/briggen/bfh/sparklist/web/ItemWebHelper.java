package ch.briggen.bfh.sparklist.web;

import java.util.Calendar;
import java.util.Date;

//import java.sql.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Item;
import ch.briggen.bfh.sparklist.domain.ItemRepository;
import spark.Request;

class ItemWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(ItemWebHelper.class);

	public static Item itemFromWeb(Request request) {

		return new Item(Integer.parseInt(request.queryParams("itemDetail.id")), request.queryParams("itemDetail.name"),
				Integer.parseInt(request.queryParams("itemDetail.stock")),
				request.queryParams("itemDetail.description"),
				Integer.parseInt(request.queryParams("itemDetail.categoryId")),
				request.queryParams("itemDetail.storagelocation"),
				Integer.parseInt(request.queryParams("itemDetail.list")),
				java.sql.Date.valueOf(request.queryParams("itemDetail.expirydate")));
	}

	public static boolean checkStock(Item checkIt) {
		return checkIt.getStock() != 0 && checkIt.getStock() > 0;
	}

	public static boolean checkCatandName(Item checkName) {
		ItemRepository iRep = new ItemRepository();
		try {
			iRep.getByCatandName(checkName.getName(), checkName.getCategoryId());
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static int checkCatandNameUpdate(Item checkName) {
		ItemRepository iRep = new ItemRepository();
		try {
			Item test = iRep.getByCatandName(checkName.getName(), checkName.getCategoryId());
			return test.getId();
		} catch (Exception e) {
			return checkName.getId();
		}
	}

	public static long checkDate(Item checkMe) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);

		Date expirydate = checkMe.getExpirydate();
		return expirydate.getTime() - cal.getTimeInMillis();

	}
}
