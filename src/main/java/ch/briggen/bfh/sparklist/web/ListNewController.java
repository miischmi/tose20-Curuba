package ch.briggen.bfh.sparklist.web;

import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.List;
import ch.briggen.bfh.sparklist.domain.ListRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class ListNewController implements TemplateViewRoute {

	private final Logger log = LoggerFactory.getLogger(ListNewController.class);

	private ListRepository listRepo = new ListRepository();

	/**
	 * Erstellt eine neue Liste in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected.
	 * 
	 * Hört auf POST /list/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		List listDetail = ListWebHelper.listFromWeb(request);
		HashMap<String, Object> model = new HashMap<String, Object>();
		ArrayList<String> errors = new ArrayList<String>();

		if (ListWebHelper.checkListName(listDetail)) {
			errors.add("Es exisitert bereits eine Liste mit diesem Namen!");
			response.redirect("/list?" + sumUrl(errors));
		}

		else if (listDetail != null) {
			log.trace("POST /list/new mit listDetail " + listDetail);
			// insert gibt die von der DB erstellte id zurück.
			int id = listRepo.insert(listDetail);

			// die neue Id wird dem Redirect als Parameter hinzugefügt
			// der redirect erfolgt dann auf /lsit?id=432932
			response.redirect("/list?id=" + id);

		}
		return null;
	}

	private String sumUrl(ArrayList<String> errors) {
		String url = "";
		for (int i = 0; i < errors.size(); i++) {
			url += "error=" + errors.get(i) + "&";
		}
		return url;
	}

}
