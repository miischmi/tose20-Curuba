package ch.briggen.bfh.sparklist.web;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Category;
import ch.briggen.bfh.sparklist.domain.CategoryRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelne Kategorien
 * 
 * @author Michelle Schmid
 */

public class CategoryNewController implements TemplateViewRoute {

	private final Logger log = LoggerFactory.getLogger(CategoryNewController.class);

	private CategoryRepository categoryRepo = new CategoryRepository();

	/**
	 * Erstellt eine neue Kategorie in der DB. Die id wird von der Datenbank
	 * erstellt. Bei Erfolg wird wieder auf die Detailseite des Items redirected
	 * 
	 * Hört auf POST /list/item/category/new
	 * 
	 * @return Redirect zurück zur Detailmaske des Items
	 */
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idList = request.queryParams("list");
		String idString = request.queryParams("id");
		Category categoryDetail = CategoryWebHelper.catFromWeb(request);
		ArrayList<String> errors = new ArrayList<String>();

		if (CategoryWebHelper.checkCatName(categoryDetail)) {
			errors.add("Diese Kategorie existiert bereits!");
			if (idString == null && idList != null) {
				response.redirect("/list/item/category?c=" + "&list=" + idList + "&" + sumUrl(errors));
			} else if (idString != null && idList != null) {
				response.redirect(
						"/list/item/category?c=" + "&list=" + idList + "&id=" + idString + "&" + sumUrl(errors));
			}
		} else if (categoryDetail != null) {
			log.trace("POST /list/item/category/new mit categoryDetail " + categoryDetail);
			categoryRepo.insert(categoryDetail);
			if (idString == null && idList != null) {
				response.redirect("/list/item?list=" + idList);
			} else if (idString != null && idList != null) {
				response.redirect("/list/item?list=" + idList + "&id=" + idString);
			}

		}
		return null;
	}

	private String sumUrl(ArrayList<String> errors) {
		String url = "";
		for (int i = 0; i < errors.size(); i++) {
			url += "error=" + errors.get(i) + "&";
		}
		return url;
	}

}
