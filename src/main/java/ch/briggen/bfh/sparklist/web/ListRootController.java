package ch.briggen.bfh.sparklist.web;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.*;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class ListRootController implements TemplateViewRoute {

	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(ListManagementRootController.class);

	ListRepository listRepository = new ListRepository();
	PersonRepository personRepository = new PersonRepository();

	/**
	 * Liefert die Liste als Root-Seite "/" zurück
	 */
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		ArrayList<List> lists = new ArrayList<List>(listRepository.getAllLists());
		ArrayList<String> infos = new ArrayList<String>();
		// Durchiterieren der Items-Tabelle, um die abgeloffenen Items zu finden.
		for (int i = 0; i < lists.size(); i++) {
			List list1 = lists.get(i);
			ArrayList<Item> items = new ArrayList<Item>(list1.getItems());
			for (int j = 0; j < items.size(); j++) {
				Item item1 = items.get(j);
				if (ItemWebHelper.checkDate(item1) == 86400000) {
					infos.add("Das Item " + item1.getName() + " auf der Liste \"" + list1.getName()
							+ "\" läuft morgen ab.");
				} if (ItemWebHelper.checkDate(item1) < 0) {
					infos.add("Das Item " + item1.getName() + " auf der Liste \"" + list1.getName()
							+ "\" ist vor langer Zeit abgelaufen.");
				} else if  (ItemWebHelper.checkDate(item1) < 86400000) {
					infos.add("Das Item " + item1.getName() + " auf der Liste \"" + list1.getName()
					+ "\" läuft heute ab!.");
				}
			}
		}

		// Items werden geladen und die Collection dann für das Template unter dem namen
		// "list" bereitgestellt
		// Das Template muss dann auch den Namen "list" verwenden.
		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("list", lists);
		model.put("infos", infos);
		model.put("personList", personRepository.getAllPersons());
		return new ModelAndView(model, "listRootTemplate");

	}

}
