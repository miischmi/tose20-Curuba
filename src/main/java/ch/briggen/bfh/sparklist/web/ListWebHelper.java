package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Category;
import ch.briggen.bfh.sparklist.domain.CategoryRepository;
import ch.briggen.bfh.sparklist.domain.Item;
import ch.briggen.bfh.sparklist.domain.ItemRepository;
import ch.briggen.bfh.sparklist.domain.List;
import ch.briggen.bfh.sparklist.domain.ListRepository;
import spark.Request;

public class ListWebHelper {

	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(ListWebHelper.class);

	public static List listFromWeb(Request request) {

		return new List(Integer.parseInt(request.queryParams("listDetail.id")), request.queryParams("listDetail.name"),
				request.queryParams("listDetail.description"));
	}

	public static boolean checkListName(List checkList) {
		ListRepository listRep = new ListRepository();
		try {
			listRep.getByName(checkList.getName());
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static int checkListNameUpdate(List checkList) {
		ListRepository listRep = new ListRepository();
		try {
			List test = listRep.getByName(checkList.getName());
			return test.getId();
		} catch (Exception e) {
			return checkList.getId();
		}
	}
}
