package ch.briggen.bfh.sparklist.web;

import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Item;
import ch.briggen.bfh.sparklist.domain.ItemRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Items
 * 
 * @author Marcel Briggen
 *
 */

public class ItemNewController implements TemplateViewRoute {

	private final Logger log = LoggerFactory.getLogger(ItemNewController.class);

	private ItemRepository itemRepo = new ItemRepository();

	/**
	 * Erstellt ein neues Item in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /item&id=99 wenn
	 * die id 99 war.)
	 * 
	 * Hört auf POST /list/item/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Item itemDetail = ItemWebHelper.itemFromWeb(request);
		ArrayList<String> errors = new ArrayList<String>();

		if (!ItemWebHelper.checkStock(itemDetail)) {
			errors.add("Stock darf nicht 0 oder kleiner als 0 sein");
		}
		if (ItemWebHelper.checkCatandName(itemDetail)) {
			errors.add("Dieses Item existiert bereits in dieser Kategorie");
		}
		if (ItemWebHelper.checkDate(itemDetail) < 0) {
			errors.add("Ablaufdatum kann nicht in der Vergangenheit liegen");
		}
		if (errors.size() > 0) {
			response.redirect("/list/item?list=" + itemDetail.getList() + "&" + sumUrl(errors));
		} else {

			log.trace("POST /item/new mit itemDetail " + itemDetail);
			// insert gibt die von der DB erstellte id zurück.
			int id = itemRepo.insert(itemDetail);

			// die neue Id wird dem Redirect als Parameter hinzugefügt
			// der redirect erfolgt dann auf /item?id=432932
			response.redirect("/list/item?id=" + id + "&list=" + itemDetail.getList());

		}
		return null;
	}

	private String sumUrl(ArrayList<String> errors) {
		String url = "";
		for (int i = 0; i < errors.size(); i++) {
			url += "error=" + errors.get(i) + "&";
		}
		return url;
	}

}
