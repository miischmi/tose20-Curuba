package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.*;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class CategoryEditController implements TemplateViewRoute {

	private final Logger log = LoggerFactory.getLogger(CategoryEditController.class);

	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		String idList = request.queryParams("list");
		HashMap<String, Object> model = new HashMap<String, Object>();
		String[] errors = request.queryParamsValues("error");

		if (null == idString || idString.equals("0")) {
			log.trace("GET /category für INSERT mit list" + idList);
			model.put("postAction", "/list/item/category/new?list=" + idList);
			model.put("backButton", "/list/item?list=" + idList);
			model.put("categoryDetail", new Category());
			model.put("errors", errors);
		} else {
			log.trace("GET /category für INSERT mit itemId " + idString + "und list" + idList);
			model.put("postAction", "/list/item/category/new?list=" + idList + "&id=" + idString);
			model.put("backButton", "/list/item?list=" + idList + "&id=" + idString);
			model.put("categoryDetail", new Category());
			model.put("errors", errors);
		}
		return new ModelAndView(model, "categoryDetailTemplate");
	}
}
