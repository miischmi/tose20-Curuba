package ch.briggen.bfh.sparklist.web;

import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.CategoryRepository;
import ch.briggen.bfh.sparklist.domain.Category;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Kategorien
 * 
 * @author Michelle Schmid
 *
 */

public class CategoryDeleteController implements TemplateViewRoute {

	private final Logger log = LoggerFactory.getLogger(CategoryDeleteController.class);

	private CategoryRepository categoryRepo = new CategoryRepository();

	/**
	 * Löscht die Kategorie mit der übergebenen id in der Datenbank
	 * 
	 * 
	 * Hört auf GET /category/delete
	 * 
	 * @return Redirect zurück zur Übersicht
	 */
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
		ArrayList<Category> categories = new ArrayList<Category>(categoryRepo.getAll());
		ArrayList<String> errors = new ArrayList<String>();

		if (id == null) {
			log.trace("show all categories");
			model.put("categories", categories);

		} else if (CategoryWebHelper.checkCat(Integer.parseInt(id)) > 0) {
			errors.add("Es existieren noch Items mit dieser Kategorie!");
			model.put("categories", categories);
			model.put("errors", errors);
		} else {
			log.trace("GET /category/delete mit id " + id);
			int longId = Integer.parseInt(id);
			categoryRepo.delete(longId);
			categories = new ArrayList<Category>(categoryRepo.getAll());
			model.put("categories", categories);
		}
		return new ModelAndView(model, "categoryTemplate");
	}
}
