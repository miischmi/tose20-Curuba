package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ch.briggen.bfh.sparklist.domain.Category;
import ch.briggen.bfh.sparklist.domain.CategoryRepository;
import ch.briggen.bfh.sparklist.domain.Item;
import ch.briggen.bfh.sparklist.domain.ItemRepository;
import spark.Request;

public class CategoryWebHelper {

	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(CategoryWebHelper.class);

	public static Category catFromWeb(Request request) {

		return new Category(Integer.parseInt(request.queryParams("categoryDetail.id")),
				request.queryParams("categoryDetail.name"), request.queryParams("categoryDetail.description"));
	}

	public static int checkCat(int catId) {
		ItemRepository iRep = new ItemRepository();
		return iRep.getByCategory(catId).size();
	}

	public static boolean checkCatName(Category checkCat) {
		CategoryRepository categoryRep = new CategoryRepository();
		try {
			categoryRep.getByName(checkCat.getName());
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
