package ch.briggen.bfh.sparklist.web;

import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.*;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class ListUpdateController implements TemplateViewRoute {

	private final Logger log = LoggerFactory.getLogger(ListUpdateController.class);

	private ListRepository ListRepo = new ListRepository();

	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		List listDetail = ListWebHelper.listFromWeb(request);
		ArrayList<String> errors = new ArrayList<String>();

		if (ListWebHelper.checkListNameUpdate(listDetail) != listDetail.getId()) {
			errors.add("Es exisitert bereits eine Liste mit diesem Namen!");
			response.redirect("/list?id=" + listDetail.getId() + "&" + sumUrl(errors));
		} else if (listDetail != null) {
			log.trace("POST /list/update mit listDetail " + listDetail);
			ListRepo.save(listDetail);
			response.redirect("/list?id=" + listDetail.getId());
		}
		return null;
	}

	private String sumUrl(ArrayList<String> errors) {
		String url = "";
		for (int i = 0; i < errors.size(); i++) {
			url += "error=" + errors.get(i) + "&";
		}
		return url;
	}
}
