package ch.briggen.bfh.sparklist.web;

import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Item;
import ch.briggen.bfh.sparklist.domain.ItemRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Items 
 * @author Marcel Briggen
 *
 */

public class ItemUpdateController implements TemplateViewRoute {

	private final Logger log = LoggerFactory.getLogger(ItemUpdateController.class);

	private ItemRepository itemRepo = new ItemRepository();

	/**
	 * Schreibt das geänderte Item zurück in die Datenbank Bei Erfolg erfolgt ein
	 * REDIRECT zurück auf die Detailseite (/item) mit der Item-id als Parameter mit
	 * dem namen id. Validierung: Im Fehlerfall wird eine durch Spring eine
	 * Fehlerseite generiert.
	 * 
	 * Hört auf POST /list/item/update
	 * 
	 * @return redirect nach /item: via Browser wird /list/item aufgerufen, also editItem
	 *         weiter oben und dann das Detailformular angezeigt.
	 */

	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Item itemDetail = ItemWebHelper.itemFromWeb(request);
		HashMap<String, Object> model = new HashMap<String, Object>();
		ArrayList<String> errors = new ArrayList<String>();

		if (!ItemWebHelper.checkStock(itemDetail)) {
			errors.add("Stock darf nicht 0 oder kleiner als 0 sein");
		}
		if (ItemWebHelper.checkCatandNameUpdate(itemDetail) != itemDetail.getId()) {
			errors.add("Dieses Item existiert bereits in dieser Kategorie");
		}
		if (ItemWebHelper.checkDate(itemDetail) < 0) {
			errors.add("Ablaufdatum kann nicht in der Vergangenheit liegen");
		}
		if (errors.size() > 0) {
			response.redirect(
					"/list/item?id=" + itemDetail.getId() + "&list=" + itemDetail.getList() + "&" + sumUrl(errors));
		} else {
			log.trace("POST /item/update mit itemDetail " + itemDetail);

			// Speichern des Items in dann den Parameter für den Redirect abfüllen
			// der Redirect erfolgt dann z.B. auf /item&id=3 (wenn itemDetail.getId == 3
			// war)
			itemRepo.save(itemDetail);
			response.redirect("/list/item?id=" + itemDetail.getId() + "&list=" + itemDetail.getList());
		}

		return null;
	}

	private String sumUrl(ArrayList<String> errors) {
		String url = "";
		for (int i = 0; i < errors.size(); i++) {
			url += "error=" + errors.get(i) + "&";
		}
		return url;
	}
}
