package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * 
 * Klasse um Personen in die DB zu speichern, löschen etc.
 */

public class PersonRepository {

	private final static Logger log = LoggerFactory.getLogger(ItemRepository.class);

	public Collection<Person> getAllPersons() {
		log.trace("getAll persons");

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("select * from person");
			ResultSet rs = stmt.executeQuery();
			return mapPersons(rs);
		} catch (SQLException e) {
			String msg = "SQL error while retreiving all persons. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	public static int insertPerson(Person p) {

		log.trace("insert person " + p);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement(
					"insert into person (person_firstname, person_lastname, email, impressum) values  (?,?,?,?)",
					PreparedStatement.RETURN_GENERATED_KEYS);
			stmt.setString(1, p.getFirstname());
			stmt.setString(2, p.getLastname());
			stmt.setString(3, p.getEmail());
			stmt.setInt(4, p.getImpressum());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			int id = key.getInt(1);
			return id;
		} catch (SQLException e) {
			String msg = "SQL error while inserting person " + p;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	private static Collection<Person> mapPersons(ResultSet rs) throws SQLException {

		LinkedList<Person> personList = new LinkedList<Person>();
		while (rs.next()) {

			Person p = new Person(rs.getInt("person_id"), rs.getString("person_firstname"),
					rs.getString("person_lastname"), rs.getString("email"), rs.getInt("impressum"));
			personList.add(p);
		}
		return personList;
	}

	public static void addTeam() {

		log.trace("insert team in DB");

		Person michelle = new Person("Michelle", "Schmid", "michelle.schmid@students.bfh.ch", 1);
		Person carol = new Person("Carol", "Gutknecht", "carolann.gutknecht@students.bfh.ch", 1);
		Person marcel = new Person("Marcel", "Ernst", "marcel.ernst@students.bfh.ch", 1);
		Person denise = new Person("Denise", "Schärer", "denise.schaerer@students.bfh.ch", 1);
		Person dominique = new Person("Dominique", "Marty", "dominiquesebastian.marty@students.bfh.ch", 1);
		Person jonas = new Person("Jonas", "Baschung", "jonasmartin.baschung@students.bfh.ch", 1);

		insertPerson(michelle);
		insertPerson(carol);
		insertPerson(marcel);
		insertPerson(denise);
		insertPerson(dominique);
		insertPerson(jonas);

	}

}
