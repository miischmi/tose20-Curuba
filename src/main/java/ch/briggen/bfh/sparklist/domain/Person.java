package ch.briggen.bfh.sparklist.domain;

/*
 * 
 * Klasse Person fürs Impressum
 * 
 */

public class Person {

	private Integer id;
	private String firstname;
	private String lastname;
	private String email;
	private Integer impressum;
	private String fullName;

	public Person(Integer id, String firstname, String lastname, String email, Integer impressum) {
		super();
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.impressum = impressum;
		this.fullName = firstname + " " + lastname;
	}

	public Person(String firstname, String lastname, String email, Integer impressum) {
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.impressum = impressum;
		this.fullName = firstname + " " + lastname;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getImpressum() {
		return impressum;
	}

	public void setImpressum(Integer impressum) {
		this.impressum = impressum;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

}
