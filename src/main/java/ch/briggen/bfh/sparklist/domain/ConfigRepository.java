package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfigRepository {

	private final static Logger log = LoggerFactory.getLogger(ItemRepository.class);

	public Collection<Config> getConfig() {
		log.trace("getAll configs");

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("select * from Config");
			ResultSet rs = stmt.executeQuery();
			return mapConfig(rs);
		} catch (SQLException e) {
			String msg = "SQL error while retreiving all configs. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Speichert eine neue Konfiguration
	 * 
	 * @param i neu zu erstellende Konfiguration
	 * @return Liefert die von der DB generierte id der neuen Konfiguration zurück
	 */
	public static int insertConfig(Config con) {

		log.trace("insert config");

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("insert into config (config_firstStartup) values (?)",
					PreparedStatement.RETURN_GENERATED_KEYS);
			stmt.setBoolean(1, con.getFirstStartup());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			int id = key.getInt(1);
			return id;
		} catch (SQLException e) {
			String msg = "SQL error while inserting config " + con;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Updated die Konfiguration
	 * 
	 * @param i = die Id der Konfiguration die man aktualisieren will
	 */
	public void saveConfig(Config con) {
		log.trace("Config: save " + con);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("update config set config_firstStartup=? where config_id=?");
			stmt.setBoolean(1, con.getFirstStartup());
			stmt.setInt(2, con.getId());
			stmt.executeUpdate();
		} catch (SQLException e) {
			String msg = "SQL error while updating config " + con;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	private static Collection<Config> mapConfig(ResultSet rs) throws SQLException {

		LinkedList<Config> configList = new LinkedList<Config>();
		while (rs.next()) {

			Config i = new Config(rs.getInt("config_id"), rs.getBoolean("config_firststartup"));
			configList.add(i);
		}
		return configList;
	}

	public static void createConfig() {
		Config theConfig = new Config(true);

		insertConfig(theConfig);

	}

}
