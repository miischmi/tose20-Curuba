package ch.briggen.bfh.sparklist.domain;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import ch.briggen.sparkbase.JdbcSparkApp;

public class JdbcRepositoryHelper {

	private static DataSource alternativeDataSource = null;

	public static synchronized void overrideDefalutDataSource(DataSource javaxDataSource) {
		alternativeDataSource = javaxDataSource;
	}

	public static synchronized Connection getConnection() throws SQLException {
		if (null == alternativeDataSource) {
			return JdbcSparkApp.getApplication().getJdbcConnection();
		} else {
			return alternativeDataSource.getConnection();
		}
	}

}
