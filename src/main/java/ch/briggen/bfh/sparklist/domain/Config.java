package ch.briggen.bfh.sparklist.domain;

/*
 * 
 * Klasse um bestimmte Parameter zu Applikation zu persistieren
 */

public class Config {

	private Integer id;
	private Boolean firstStartup;
	private static Config instance;

	public static Config getInstance() {
		if (Config.instance == null) {
			Config.instance = new Config(1, true);
		}
		return Config.instance;
	}

	public Config(Boolean firstStartup) {
		this.firstStartup = firstStartup;
		this.id = 1;
		Config.instance = this;
	}

	public Config(Integer id, Boolean firstStartup) {
		this.id = id;
		this.firstStartup = firstStartup;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getFirstStartup() {
		return firstStartup;
	}

	public void setFirstStartup(Boolean firstStartup) {
		this.firstStartup = firstStartup;
	}

}
