package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ImpressumRepository {

	private final static Logger log = LoggerFactory.getLogger(ItemRepository.class);

	public Collection<Impressum> getImpressum() {
		log.trace("get Impressum");

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("select * from impressum");
			ResultSet rs = stmt.executeQuery();
			return mapImpressum(rs);
		} catch (SQLException e) {
			String msg = "SQL error while retreiving impressum. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	public static int insertImpressum(Impressum imp) {
		log.trace("insert impressum " + imp);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement(
					"insert into impressum (impressum_name, impressum_description) values  (?,?)",
					PreparedStatement.RETURN_GENERATED_KEYS);
			stmt.setString(1, imp.getName());
			stmt.setString(2, imp.getDescription());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			int id = key.getInt(1);
			return id;
		} catch (SQLException e) {
			String msg = "SQL error while inserting impressum " + imp;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	private static Collection<Impressum> mapImpressum(ResultSet rs) throws SQLException {

		LinkedList<Impressum> impressumList = new LinkedList<Impressum>();
		while (rs.next()) {

			Impressum imp = new Impressum(rs.getInt("impressum_id"), rs.getString("impressum_name"),
					rs.getString("impressum_description"));
			impressumList.add(imp);
		}

		return impressumList;
	}

	private static Impressum mapImpressumAnPersons(ResultSet rs) throws SQLException {

		LinkedList<Impressum> impressumList = new LinkedList<Impressum>();
		while (rs.next()) {
			int impId = rs.getInt("impressum_id");
			Collection<Person> personsOnImp = getAllPersonsForImpressum(impId);

			Impressum imp = new Impressum(rs.getInt("impressum_id"), rs.getString("impressum_name"),
					rs.getString("impressum_deescription"), personsOnImp);
			impressumList.add(imp);
		}

		return impressumList.getFirst();
	}

	public static Collection<Person> getAllPersonsForImpressum(int impId) {
		log.trace("getAll persons for impressum");

		int impressumid = impId;

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement(
					"select person_firstname, person_lastname, email, impressum from person where impressum=?");
			stmt.setInt(1, impressumid);
			ResultSet rs = stmt.executeQuery();
			return mapPersons(rs);
		} catch (SQLException e) {
			String msg = "SQL error while retreiving all persons from impressum. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	public static void createImprint() {

		Impressum impressum = new Impressum("Impressum", "Das ist das Hauptimpressum");
		insertImpressum(impressum);
	}

	private static Collection<Person> mapPersons(ResultSet rs) throws SQLException {

		LinkedList<Person> personList = new LinkedList<Person>();
		while (rs.next()) {

			Person p = new Person(rs.getInt("person_id"), rs.getString("person_firstname"),
					rs.getString("person_lastname"), rs.getString("email"), rs.getInt("impressum_id"));
			personList.add(p);
		}
		return personList;
	}

}
