package ch.briggen.bfh.sparklist.domain;

import java.util.Collection;

public class List {

	private ListRepository listRepo = new ListRepository();

	private int id;
	private String name;
	private String description;
	private Collection<Item> items;
	private Collection<List> allLists;

	public List() {
		getAllMyItems();
	}

	private void getAllMyItems() {

		Collection<Item> myItems = listRepo.getItemsByListId(this.id);

		if (myItems != null) {
			this.items = myItems;
		}

	}

	public List(String name, String description) {
		getAllMyItems();
	}

	public List(int id, String name, String description, Collection<Item> items, Collection<List> allLists) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.items = items;
		this.allLists = allLists;
	}

	public List(int id, String name, String description, Collection<Item> items) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.items = items;
	}

	public List(int id, String name, String description) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		getAllMyItems();
	}

	public Collection<List> getAllLists() {
		return allLists;
	}

	public void setAllLists(Collection<List> allLists) {
		this.allLists = allLists;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Collection<Item> getItems() {
		return items;
	}

	public void setItems(Collection<Item> items) {
		this.items = items;
	}

}
