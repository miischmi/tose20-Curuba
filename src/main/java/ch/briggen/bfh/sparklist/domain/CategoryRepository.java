package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CategoryRepository {

	private final Logger log = LoggerFactory.getLogger(ItemRepository.class);

	public Collection<Category> getAll() {
		log.trace("getAll categories");

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn
					.prepareStatement("select category_id, category_name, category_description from categories");
			ResultSet rs = stmt.executeQuery();
			return mapCategories(rs);

		} catch (SQLException e) {
			String msg = "SQL error while retreiving all categories. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	public Category getById(int id) {
		log.trace("Category: getById " + id);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement(
					"select category_id, category_name, category_description from categories where category_id=?");
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapCategories(rs).iterator().next();
		} catch (SQLException e) {
			String msg = "SQL error while retreiving categories by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Liefert alle Kategorie mit dem angegebenen Namen
	 * 
	 * @param name
	 * @return Kategorie mit dem Namen "name"
	 */
	public Category getByName(String name) throws RepositoryException {
		log.trace("Category: getByName " + name);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement(
					"select category_id, category_name, category_description from categories where category_name=?");
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			return mapCategories(rs).iterator().next();
		} catch (SQLException e) {
			String msg = "SQL error while retreiving categories by name " + name;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Löscht die Kategorie mit der angegebenen Id von der DB
	 * 
	 * @param id der Kategorie die man löschen will
	 */
	public void delete(int id) {
		log.trace("delete category with id " + id);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("delete from categories where category_id=?");
			stmt.setInt(1, id);
			stmt.executeUpdate();
		} catch (SQLException e) {
			String msg = "SQL error while deleteing category by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Speichert eine neue Kategorie
	 * 
	 * @param i neu zu erstellende Kategorie
	 * @return Liefert die von der DB generierte id der neuen Kategorie zurück
	 */
	public int insert(Category i) {

		log.trace("insert category");

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement(
					"insert into categories (category_name, category_description) values (?,?)",
					PreparedStatement.RETURN_GENERATED_KEYS);
			stmt.setString(1, i.getName());
			stmt.setString(2, i.getDescription());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			int id = key.getInt(1);
			return id;
		} catch (SQLException e) {
			String msg = "SQL error while inserting categories " + i;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Helper to convert results from db in to category objects.
	 * 
	 * @author Jonas Baschung
	 * @throws SQLException
	 *
	 */
	private static Collection<Category> mapCategories(ResultSet rs) throws SQLException {

		LinkedList<Category> list = new LinkedList<Category>();
		while (rs.next()) {
			Category i = new Category(rs.getInt("category_id"), rs.getString("category_name"),
					rs.getString("category_description"));
			list.add(i);
		}
		return list;
	}

	/**
	 * Inserts categories into DB, is called on startup
	 * 
	 * @Author Jonas Baschung
	 */
	public void CreateCategories() {

		log.trace("Inserting Categories");

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn
					.prepareStatement("INSERT INTO categories (category_name, category_description)\n" + "VALUES\n"
							+ "('Lebensmittel (Raumtemperatur)', 'Stuff that goes into the kitchen' ),\n"
							+ "('Lebensmittel (-20°C)', 'Stuff that goes into the freezer' ),\n"
							+ "('Lebensmittel (4°C)', 'Stuff that goes into the fridge' ),\n"
							+ "('Toiletries', 'Stuff for your personal care' ),\n"
							+ "('Putzmittel', 'Stuff that you need to clean the kitchen' )");

			int rs = stmt.executeUpdate();
			log.trace(Integer.toString(rs) + " Zeilen hinzugefügt.");

		} catch (SQLException e) {
			String msg = "SQL error while inserting categories. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

}
