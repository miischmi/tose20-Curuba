package ch.briggen.bfh.sparklist.domain;

import java.sql.Date;
import java.util.Collection;
import java.util.List;

/**
 * Einzelner Eintrag in der Liste mit Name und Menge (und einer eindeutigen Id)
 * 
 * @author Marcel Briggen
 *
 */
public class Item {

	private CategoryRepository categoryRepo = new CategoryRepository();

	private int id;
	private String name;
	private int stock;
	private String description;
	private int categoryId;
	private Category category;
	private List<String> categoryNames;
	private List<Category> categories;
	private String storagelocation;
	private Date expirydate;
	private int list;

	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Item() {
		AssignCategories();
	}

	public Item(int list) {
		AssignCategories();
		this.list = list;
	}

	/**
	 * Konstruktor
	 * 
	 * @param id              Eindeutige Id
	 * @param name            Name des eintrags in der Liste
	 * @param quantity        Menge
	 * @param description     Freitext
	 * @param category        Eine von 5 vordefinierten Kategorien
	 * @param storagelocation Einer von 8 möglichen Lagerorten
	 * @param expirydate      Ablaufdatum
	 */
	public Item(int id, String name, int stock, String description, int categoryId, String storagelocation, int list,
			Date expirydate) {
		this.id = id;
		this.name = name;
		this.stock = stock;
		this.description = description;
		this.categoryId = categoryId;
		this.storagelocation = storagelocation;
		this.list = list;
		this.expirydate = expirydate;
		AssignCategories();
		AssignCategoryName();

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getList() {
		return list;
	}

	public void setList(int list) {
		this.list = list;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public String getStoragelocation() {
		return storagelocation;
	}

	public void setStoragelocation(String storagelocation) {
		this.storagelocation = storagelocation;
	}

	public Date getExpirydate() {
		return expirydate;
	}

	public void setExpirydate(Date expirydate) {
		this.expirydate = expirydate;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return String.format(
				"Item:{id: %d; name: %s; quantity: %d, description: %s; category: %s; storagelocation: %s; expiry date}",
				id, name, stock, description, categoryId, storagelocation, expirydate);
	}

	public List<Category> getCategories() {
		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

	/**
	 * This method is used to assign category names to the class itself when
	 * displayed in a controller
	 * 
	 * @author Jonas Baschung
	 */
	public void AssignCategoryNames() {

		Collection<Category> myCategories = categoryRepo.getAll();

		if (myCategories != null) {

			for (Category cat : myCategories) {
				if (categoryNames.contains(cat.getName())) {
				} else {
					this.categoryNames.add(cat.getName());
				}
			}
		}
	}

	/**
	 * This method is used to assign categories to the class itself when displayed
	 * in a controller
	 * 
	 * @author Jonas Baschung
	 */
	private void AssignCategories() {

		this.categories = (List<Category>) categoryRepo.getAll();
	}

	/**
	 * This method is used to assign the category name to the class itself when
	 * displayed in a controller
	 * 
	 * @author Jonas Baschung
	 */
	private void AssignCategoryName() {

		this.category = categoryRepo.getById(this.categoryId);
	}

}
