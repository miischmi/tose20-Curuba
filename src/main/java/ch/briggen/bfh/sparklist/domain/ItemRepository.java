package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Repository für alle Items. Hier werden alle Funktionen für die DB-Operationen
 * zu Items implementiert
 * 
 * @author Marcel Briggen
 *
 */

public class ItemRepository {

	private final static Logger log = LoggerFactory.getLogger(ItemRepository.class);

	/**
	 * Liefert alle items in der Datenbank
	 * 
	 * @return Collection aller Items
	 */
	public Collection<Item> getAll() {
		log.trace("getAll");
		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement(
					"select item_id, item_name, stock, item_description, category, storagelocation, list, expirydate from items");
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);
		} catch (SQLException e) {
			String msg = "SQL error while retreiving all items. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert alle Items mit dem angegebenen Namen
	 * 
	 * @param name
	 * @return Collection mit dem Namen "name"
	 */
	public Collection<Item> getByName(String name) throws RepositoryException {
		log.trace("getByName " + name);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement(
					"select item_id, item_name, stock, item_description, category, storagelocation, list, expirydate from items where item_name=?");
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);
		} catch (SQLException e) {
			String msg = "SQL error while retreiving items by name " + name;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Liefert alle Items mit der angegebenen Kategorie
	 * 
	 * @param name
	 * @return Collection mit der Kategorie "id"
	 */
	public Collection<Item> getByCategory(int id) throws RepositoryException {
		log.trace("getByCategory " + id);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement(
					"select item_id, item_name, stock, item_description, category, storagelocation, list, expirydate from items where category=?");
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);
		} catch (SQLException e) {
			String msg = "SQL error while retreiving items by name " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Liefert das Item mit dem angegebenen Namen und Kategorie
	 * 
	 * @param name
	 * @return Item mit dem Namen "name" und der Kategorie "category"
	 */
	public Item getByCatandName(String name, int category) {
		log.trace("getByCatName " + name + ", " + category);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement(
					"select item_id, item_name, stock, item_description, category, storagelocation, list, expirydate from items where item_name=? and category=?");
			stmt.setString(1, name);
			stmt.setInt(2, category);
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs).iterator().next();
		} catch (SQLException e) {
			String msg = "SQL error while retreiving items by name and category" + name + ", " + category;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Liefert das Item mit der übergebenen Id
	 * 
	 * @param id id des Item
	 * @return Item oder NULL
	 */
	public Item getById(int id) {
		log.trace("getById " + id);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement(
					"select item_id, item_name, stock, item_description, category, storagelocation, list, expirydate from items where item_id=?");
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs).iterator().next();
		} catch (SQLException e) {
			String msg = "SQL error while retreiving items by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Speichert das übergebene item in der Datenbank. UPDATE.
	 * 
	 * @param i
	 */
	public void save(Item i) {
		log.trace("save " + i);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement(
					"update items set item_name=?, stock=?, item_description=?, category=?, storagelocation=?, list=?,expirydate=? where item_id=?");
			stmt.setString(1, i.getName());
			stmt.setInt(2, i.getStock());
			stmt.setString(3, i.getDescription());
			stmt.setInt(4, i.getCategoryId());
			stmt.setString(5, i.getStoragelocation());
			stmt.setInt(6, i.getList());
			stmt.setDate(7, i.getExpirydate());
			stmt.setInt(8, i.getId());
			stmt.executeUpdate();
		} catch (SQLException e) {
			String msg = "SQL error while updating item " + i;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Löscht das Item mit der angegebenen Id von der DB
	 * 
	 * @param id Item ID
	 */
	public void delete(int id) {
		log.trace("delete " + id);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("delete from items where item_id=?");
			stmt.setInt(1, id);
			stmt.executeUpdate();
		} catch (SQLException e) {
			String msg = "SQL error while deleteing items by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Speichert das angegebene Item in der DB. INSERT.
	 * 
	 * @param i neu zu erstellendes Item
	 * @return Liefert die von der DB generierte id des neuen Items zurück
	 */
	public int insert(Item i) {

		log.trace("insert " + i);

		// Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement(
					"insert into items (item_name, stock, item_description, category, storagelocation, list, expirydate) values (?,?,?,?,?,?,?)",
					PreparedStatement.RETURN_GENERATED_KEYS);
			stmt.setString(1, i.getName());
			stmt.setInt(2, i.getStock());
			stmt.setString(3, i.getDescription());
			stmt.setInt(4, i.getCategoryId());
			stmt.setString(5, i.getStoragelocation());
			stmt.setInt(6, i.getList());
			stmt.setDate(7, i.getExpirydate());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			int id = key.getInt(1);
			return id;
		} catch (SQLException e) {
			String msg = "SQL error while inserting item " + i;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Helper zum konvertieren der Resultsets in Item-Objekte. Siehe getByXXX
	 * Methoden.
	 * 
	 * @author Marcel Briggen
	 * @throws SQLException
	 *
	 */
	private static Collection<Item> mapItems(ResultSet rs) throws SQLException {

		LinkedList<Item> list = new LinkedList<Item>();
		while (rs.next()) {

			Item i = new Item(rs.getInt("item_id"), rs.getString("item_name"), rs.getInt("stock"),
					rs.getString("item_description"), rs.getInt("category"), rs.getString("storagelocation"),
					rs.getInt("list"), rs.getDate("expirydate"));
			list.add(i);
		}
		return list;
	}

	// Method used to fill DB with items if needed. Great for Debugging.
	public static void putSomeApplesInDB() {

		log.trace("Inserting some Apples into DB");

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement(
					"insert into items (item_name, stock, item_description, category, storagelocation, list, expirydate)\n"
							+ "VALUES\n"
							+ "('Apfel1', 23, 'This is an apple', 2, 'Somewhere nice', 1, '2020-05-20 00:00:00'),\n"
							+ "('Apfel2', 12, 'This is an apple', 2, 'Somewhere nice', 1, '2020-05-10 00:00:00'),\n"
							+ "('Apfel3', 14, 'This is an apple', 2, 'Somewhere nice', 1, '2020-04-28 00:00:00'),\n"
							+ "('Apfel4', 18, 'This is an apple', 2, 'Somewhere nice', 1, '2020-04-28 00:00:00'),\n"
							+ "('Apfel5', 44, 'This is an apple', 2, 'Somewhere nice', 1, '2020-04-28 00:00:00'),\n"
							+ "('Apfel6', 54, 'This is an apple', 2, 'Somewhere nice', 2, '2020-04-28 00:00:00'),\n"
							+ "('Apfel7', 14, 'This is an apple', 2, 'Somewhere nice', 2, '2020-04-28 00:00:00'),\n"
							+ "('Apfel8', 52, 'This is an apple', 2, 'Somewhere nice', 2, '2020-04-28 00:00:00'),\n"
							+ "('Apfel9', 31, 'This is an apple', 2, 'Somewhere nice', 2, '2020-04-28 00:00:00'),\n"
							+ "('Apfel10',19, 'This is an apple', 2, 'Somewhere nice', 2, '2020-04-28 00:00:00')");

			int rs = stmt.executeUpdate();
			log.trace(Integer.toString(rs) + " Zeilen hinzuefügt.");

		} catch (SQLException e) {
			String msg = "SQL error while Inserting apples. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

}
