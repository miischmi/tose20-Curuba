package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListRepository {

	private final static Logger log = LoggerFactory.getLogger(ItemRepository.class);

	public Collection<List> getAllLists() {
		log.trace("getAll lists");

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("select list_id, list_name, list_description from lists");
			ResultSet rs = stmt.executeQuery();
			return mapList(rs);
		} catch (SQLException e) {
			String msg = "SQL error while retreiving all lists. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Gets List by Id
	 * 
	 * @author Jonas
	 * @throws SQLException
	 */
	public List getById(int id) {
		log.trace("Get List with Items of the list with id: " + id);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn
					.prepareStatement("select list_id, list_name, list_description from lists where list_id=?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapList(rs).iterator().next();
		} catch (SQLException e) {
			String msg = "SQL error while retreiving list by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Liefert alle Listen mit dem angegebenen Namen
	 * 
	 * @param name
	 * @return Liste mit dem Namen "name"
	 */
	public List getByName(String name) throws RepositoryException {
		log.trace("List: getByName " + name);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn
					.prepareStatement("select list_id, list_name, list_description from lists where list_name=?");
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			return mapList(rs).iterator().next();
		} catch (SQLException e) {
			String msg = "SQL error while retreiving lists by name " + name;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Gets List by Id
	 * 
	 * @author Jonas
	 * @throws SQLException
	 */
	public Collection<Item> getItemsByListId(int id) {
		log.trace("Get Items from the list with id: " + id);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement(
					"select item_id, item_name, stock, item_description, category, storagelocation, list, expirydate from items where list=?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);
		} catch (SQLException e) {
			String msg = "SQL error while retreiving items from list by id with id" + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Gets List by Id (and and the items of the list)
	 * 
	 * @author Jonas
	 * @throws SQLException
	 */
	public List getByIdWithItems(int id) {
		log.trace("Get List with Items of the list with id: " + id);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement(
					"SELECT * FROM Lists INNER JOIN Items ON Items.list=lists.list_id WHERE lists.list_id = ?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapListAndItems(rs);
		} catch (SQLException e) {
			String msg = "SQL error while retreiving list with items by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Updated die Liste
	 * 
	 * @param i = die Id der Liste die man aktualiseren will
	 */
	public void save(List i) {
		log.trace("List: save " + i);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn
					.prepareStatement("update lists set list_name=?, list_description=? where list_id=?");
			stmt.setString(1, i.getName());
			stmt.setString(2, i.getDescription());
			stmt.setInt(3, i.getId());
			stmt.executeUpdate();
		} catch (SQLException e) {
			String msg = "SQL error while updating list " + i;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Löscht das Item mit der angegebenen Id von der DB
	 * 
	 * @param id der Liste die man löschen will
	 */
	public void delete(int id) {
		log.trace("delete list with id " + id);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("delete from items where list=?");
			stmt.setInt(1, id);
			stmt.executeUpdate();
		} catch (SQLException e) {
			String msg = "SQL error while deleteing list by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("delete from lists where list_id=?");
			stmt.setInt(1, id);
			stmt.executeUpdate();
		} catch (SQLException e) {
			String msg = "SQL error while deleteing list by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Speichert eine neue Liste
	 * 
	 * @param i neu zu erstellende Liste
	 * @return Liefert die von der DB generierte id der neuen Liste zurück
	 */
	public static int insert(List i) {

		log.trace("insert list");

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement(
					"insert into lists (list_name, list_description) values (?,?)",
					PreparedStatement.RETURN_GENERATED_KEYS);
			stmt.setString(1, i.getName());
			stmt.setString(2, i.getDescription());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			int id = key.getInt(1);
			return id;
		} catch (SQLException e) {
			String msg = "SQL error while inserting list " + i;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Helper to convert results from db in to Lists objects.
	 * 
	 * @author Jonas Baschung
	 * @throws SQLException
	 *
	 */
	private static Collection<List> mapList(ResultSet rs) throws SQLException {

		LinkedList<List> list = new LinkedList<List>();
		while (rs.next()) {
			List i = new List(rs.getInt("list_id"), rs.getString("list_name"), rs.getString("list_description"));
			list.add(i);
		}
		return list;
	}

	/**
	 * Helper to convert results from db in to Lists object and items.
	 * 
	 * @author Jonas Baschung
	 * @throws SQLException
	 *
	 */
	private static List mapListAndItems(ResultSet rs) throws SQLException {
		List theList = new List();

		LinkedList<Item> listofItems = new LinkedList<Item>();

		while (rs.next()) {
			Item i = new Item(rs.getInt("item_id"), rs.getString("item_name"), rs.getInt("stock"),
					rs.getString("item_description"), rs.getInt("category"), rs.getString("storagelocation"),
					rs.getInt("list"), rs.getDate("expirydate"));
			listofItems.add(i);
		}

		if (rs.next()) {
			return theList = new List(rs.getInt("list_id"), rs.getString("list_name"), rs.getString("list_description"),
					listofItems);
		}

		return theList;

	}

	private static Collection<Item> mapItems(ResultSet rs) throws SQLException {

		LinkedList<Item> list = new LinkedList<Item>();
		while (rs.next()) {

			Item i = new Item(rs.getInt("item_id"), rs.getString("item_name"), rs.getInt("stock"),
					rs.getString("item_description"), rs.getInt("category"), rs.getString("storagelocation"),
					rs.getInt("list"), rs.getDate("expirydate"));
			list.add(i);
		}
		return list;
	}

	public static <T> Object[] convertLinkedListToArray(LinkedList<T> linkedList) {

		// Converting LinkedList to Array
		Object[] array = linkedList.toArray();

		return array;
	}

	public static void createExampleLists() {

		List vorratsListe = new List(1, "Vorratsliste", "Diese Liste wird verwendet um Vorrat zu speichern");
		List inventarListe = new List(2, "Inventarliste", "Diese Liste wird verwendet um Inventar zu speichern");

		insert(vorratsListe);
		insert(inventarListe);

	}

}
