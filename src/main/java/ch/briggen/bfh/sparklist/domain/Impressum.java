package ch.briggen.bfh.sparklist.domain;

import java.util.Collection;
import java.util.List;

/*
 * Klasse Impressum bildet das Impressum von Invendo ab.
 */

public class Impressum {

	private Integer id;
	private String name;
	private String description;
	private Collection<Person> persons;

	public Impressum(Integer id, String name, String description, Collection<Person> personsOnImp) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.persons = personsOnImp;
	}

	public Impressum(Integer id, String name, String description) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
	}

	public Impressum(String name, String description) {
		this.name = name;
		this.description = description;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Collection<Person> getPersons() {
		return persons;
	}

	public void setPersons(List<Person> persons) {
		this.persons = persons;
	}

}
