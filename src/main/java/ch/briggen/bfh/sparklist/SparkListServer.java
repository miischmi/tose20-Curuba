package ch.briggen.bfh.sparklist;

import static spark.Spark.get;
import static spark.Spark.post;

import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.CategoryRepository;
import ch.briggen.bfh.sparklist.domain.ConfigRepository;
import ch.briggen.bfh.sparklist.domain.ImpressumRepository;
import ch.briggen.bfh.sparklist.domain.ItemRepository;
import ch.briggen.bfh.sparklist.domain.ListRepository;
import ch.briggen.bfh.sparklist.domain.PersonRepository;
import ch.briggen.bfh.sparklist.web.CategoryDeleteController;
import ch.briggen.bfh.sparklist.web.CategoryEditController;
import ch.briggen.bfh.sparklist.web.CategoryNewController;
import ch.briggen.bfh.sparklist.web.ItemDeleteController;
import ch.briggen.bfh.sparklist.web.ItemEditController;
import ch.briggen.bfh.sparklist.web.ItemNewController;
import ch.briggen.bfh.sparklist.web.ItemUpdateController;
import ch.briggen.bfh.sparklist.web.ListManagementRootController;
import ch.briggen.bfh.sparklist.web.ListRootController;
import ch.briggen.bfh.sparklist.web.ListEditController;
import ch.briggen.bfh.sparklist.web.ListUpdateController;
import ch.briggen.bfh.sparklist.web.ListDeleteController;
import ch.briggen.bfh.sparklist.web.ListNewController;
import ch.briggen.sparkbase.H2SparkApp;
import ch.briggen.sparkbase.UTF8ThymeleafTemplateEngine;

public class SparkListServer extends H2SparkApp {

	final static Logger log = LoggerFactory.getLogger(SparkListServer.class);
	static CategoryRepository categoryRepo = new CategoryRepository();
	static ItemRepository itemRepo = new ItemRepository();
	static PersonRepository personRepo = new PersonRepository();
	static ImpressumRepository impRepo = new ImpressumRepository();
	static ListRepository listRepo = new ListRepository();
	static ConfigRepository configRepo = new ConfigRepository();
	static Boolean isFirstStartup;

	public static void main(String[] args) {

		for (Entry<Object, Object> property : System.getProperties().entrySet()) {
			log.debug(String.format("Property %s : %s", property.getKey(), property.getValue()));
		}

		SparkListServer server = new SparkListServer();
		server.configure();
		server.run();
		if (CheckIfFirstStartup()) {
			addExampleLists();
			createConfig();
		}
		createCategories();
		addImprint();
		addTeam();
	}

	private static void createConfig() {
		ConfigRepository.createConfig();

	}

	@Override
	protected void doConfigureHttpHandlers() {

		get("/", new ListRootController(), new UTF8ThymeleafTemplateEngine());
		get("/list", new ListEditController(), new UTF8ThymeleafTemplateEngine());
		post("/list/update", new ListUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/list/delete", new ListDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/list/new", new ListNewController(), new UTF8ThymeleafTemplateEngine());

		// get("/", new ListManagementRootController(), new
		// UTF8ThymeleafTemplateEngine());
		get("/list/item", new ItemEditController(), new UTF8ThymeleafTemplateEngine());
		post("/list/item/update", new ItemUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/list/item/delete", new ItemDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/list/item/new", new ItemNewController(), new UTF8ThymeleafTemplateEngine());

		get("/list/item/category", new CategoryEditController(), new UTF8ThymeleafTemplateEngine());
		get("/category/delete", new CategoryDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/list/item/category/new", new CategoryNewController(), new UTF8ThymeleafTemplateEngine());
	}

	/**
	 * Creates/puts categories into DB upon startup
	 * 
	 * @author Jonas Baschung
	 */
	private static void createCategories() {
		if (categoryRepo.getAll().size() == 0) {
			categoryRepo.CreateCategories();
			log.trace("Kategorien wurden hinzugefügt");

		} else {
			String msg = "Kategorien bereits vorhanden";
			log.trace(msg);

		}

	}

	/**
	 * Add team members into DB upon startup
	 * 
	 * @author Jonas Baschung
	 */
	private static void addTeam() {
		if (personRepo.getAllPersons().size() == 0) {
			PersonRepository.addTeam();
			log.trace("Team wurde hinzugefügt");

		} else {
			String msg = "Team bereits vorhanden";
			log.trace(msg);

		}
	}

	/**
	 * Adds impressum into DB upon startup
	 * 
	 * @author Jonas Baschung
	 */
	private static void addImprint() {
		if (impRepo.getImpressum().size() == 0) {
			impRepo.createImprint();
			log.trace("Impressum wurde hinzugefügt");

		} else {

			String msg = "Impressum bereits vorhanden";
			log.trace(msg);

		}

	}

	/**
	 * Adds example lists into DB upon startup
	 * 
	 * @author Jonas Baschung
	 */
	private static void addExampleLists() {
		if (listRepo.getAllLists().size() == 0) {

			listRepo.createExampleLists();

			log.trace("Beispiel-Listen wurden hinzugefügt");
		} else {

			String msg = "Beispiel-Listen bereits vorhanden";
			log.trace(msg);

		}
	}

	private static Boolean CheckIfFirstStartup() {

		if (configRepo.getConfig().size() == 0) {
			return true;
		} else {
			return false;
		}

	}

}
